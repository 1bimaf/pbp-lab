from lab_1.views import calculate_age
from django.shortcuts import render
from datetime import datetime, date
from .models import Friend
from .forms import FriendForm

mhs_name = 'Muhammad Adrian Wirakusumah'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2002, 6, 3)
npm = 2006487351

def index(request):
    response = {'name' : mhs_name,
                'age' : calculate_age(birth_date.year),
                'npm' : npm}
    return render(request, 'index_lab3.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0

def friend_list(request):
    friends = Friend.object.all().values()
    response = {'friends ': friends}
    return render(request, 'lab3_index.html', response)

def add_friend(request):
    context ={}

    form = FriendForm(request.POST or None, request.FILES or None)

    if form.is_valid():
        form.save()

    context['form']=form
    return render(request, "lab3_form.html", context)